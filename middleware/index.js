const validateFields  = require('../middleware/validar-fileds')
const isHaveRole  = require('../middleware/validar-roles')
const validarJWT  = require('../middleware/validar-jwt')
const isAdmin  = require('../middleware/validar-jwt')
const validateFileUpload  = require('../middleware/validate-file')

module.exports = {
    ...validateFields,
    ...isHaveRole,
    ...validarJWT,
    ...isAdmin,
    ...validateFileUpload

}
