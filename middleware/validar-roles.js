const { response }  = require('express')

const isAdmin = async(req, res = response, next) => {

    if (!req.user) {
        return res.status(500).json({msg:'Se quiere verificar el rol sin validar le token primero'}) 
    }
    const { role, name } = req.user

    console.log(role)

    if ( role !== 'ROLE_ADMIN') {
        return res.status(401).json({msg:`el usuario ${name} no es administrador`})
    }

    next()
}

const isHaveRole = (...roles) => {
 
    return (req, res = response, next) => {

        if (!req.user) {
            return res.status(500).json({msg:'Se quiere verificar el rol sin validar le token primero'}) 
        }

        // if role[] NO contiene el rol del usuario, entonces retorno error
        if (!roles.includes(req.user.role)) {
            return res.status(401).json({msg:`El servicio requiere uno de estos roles ${roles}`})
        }
        next()
    }
}

module.exports = {
    isAdmin,
    isHaveRole
}