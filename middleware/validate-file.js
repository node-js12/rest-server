const validateFileUpload = (req, res, next) => {
    console.log(req.files)
    if (!req.files || Object.keys(req.files).length === 0) {
        return res.status(400).json({ msg: 'Archivo no encontrado' });
    }
    next();
}

module.exports = {
    validateFileUpload
}