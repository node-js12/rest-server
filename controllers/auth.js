const { response } = require('express')
const User = require('../models/user')
const bcryptjs = require('bcryptjs')
const {generarJWT } = require('../helpers/generar-jwt')
const { verifyGoogle } = require('../helpers/google-verify')


const login = async(req, res = response) => {

    const { email, password } = req.body
    try {
        // verificar si el email existe en la bd
        const user = await User.findOne( {email} )
        if (!user) {
            return res.status(400).json({msg:`Email / Password no son correctos!`})
        }

        // verificar si el usuario esta activo
        if (!user.estado) {
            return res.status(400).json({msg:`Email / Password no son correctos!`})
        }

        // Verificar contraseña 
        const validPassword = bcryptjs.compareSync(password, user.password)

        if (!validPassword) {
            return res.status(400).json({msg:`Email / Password no son correctos! pass`})
        }
        // Generar JWT token
        const token = await generarJWT(user.id)

        res.json({
            user, token
        })

        
        
    } catch (e) {
        console.log(e)
        res.status(500).json({msg: 'Algo salio mal ! Contactese con el administrador'})
    }
}

const googleSigin = async(req, res = response) => {

    const { id_token } = req.body

    try {
        const {email, name, img} = await  verifyGoogle(id_token)

        let user = await User.findOne({ email })
        if (!user) {
            const data = {
                name,  
                email, 
                password: ':D', 
                img, 
                google: true
            }
            // tengo que crear el usuario en la BD
            user = await User(data)
            await user.save()
        }

        // si el usuario esta inactivo 
        if (!user.estado) {
            return res.status(401).json({
                msg:"Hable con el administrador usuario bloqueado!"})
        }
        // Generar JWT token
        const token = await generarJWT(user.id)
        res.json({
            user,
            token
        })

    }catch (e) {
        res.status(500).json({msg:"Token de  google no es valido"})
        console.log(e)
    }

    

}

module.exports = {
    login,
    googleSigin
}