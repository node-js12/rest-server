const path = require('path')
const fs = require('fs')
const cloudinary = require('cloudinary').v2
cloudinary.config(process.env.CLOUDINARY_URL)// autenticar con cloudinary
const { response } = require("express");
const { verificarSiExisteEnBD } = require("../helpers/uploadFile")
const { subirArchivo } = require("../helpers/uploadFile");

const getImg = async (req, res = response) => {
    const { id, coleccion } = req.params
    
    try {
        let model = await verificarSiExisteEnBD(coleccion, id)
        if (model.img)  return res.redirect(model.img);   
       
    } catch(err) {
       console.log(err)
        return res.status(400).json(err)
    }
    
     
    
}

const cargarArchivo = async (req, res = response) => {

    try {
        const nameFile = await subirArchivo(req.files,undefined,'imgs')
        res.json({ nameFile })
    } catch (error) {
        console.log(error)
        res.status(400).json({error})
    }

}

const updateImageCloudinary = async (req, res = response) => {

    const { id, coleccion } = req.params
    
    try {
        let model = await verificarSiExisteEnBD(coleccion, id)
        // limpiar imagenes previas
        if (model.img) {
            const nameArr = model.img.split('/')
            const name = nameArr[nameArr.length - 1]
            const [ public_id ] = name.split('.')
            cloudinary.uploader.destroy(public_id)
        }
        // await resp to server cloudinary
        const respo = await cloudinary.uploader.upload(req.files.archivo.tempFilePath)
        const { secure_url } = respo
        model.img = secure_url
        // await model.save();
        await model.save();
        res.status(200).json(model)
    } catch (err) {
            return res.status(400).json(err)
        }
}
const updateImage = async (req, res = response) => {
    const { id, coleccion } = req.params

    // if true, return model
    try {
        let model = await verificarSiExisteEnBD(coleccion, id)
        // limpiar imagenes previas
        if (model.img) {

            const pathImg = path.join(__dirname, '/../uploads', coleccion, model.img)

            // preguntar si la imagen ya esta cargada en el servidor
            if (fs.existsSync(pathImg)) {
                // delete img
                fs.unlinkSync(pathImg)
            }
        }
        const nameImg = await subirArchivo(req.files, undefined, coleccion)

        model.img = nameImg
        await model.save();
        res.status(200).json(model)
    } catch (err) {
        return res.status(400).json(err)
    }


}


module.exports = {
    cargarArchivo,
    updateImage,
    getImg,
    updateImageCloudinary
}