const { response } = require('express')
const User = require('../models/user')
const bcryptjs = require('bcryptjs')

const getUsers = async(req, res = response) => {

    const { limite = 5, desde = 0} = req.query
    const filterStatus = { estado: true }
    
    /* Promise.all recibe un array y ejecuta de manera simultanea 
        cada una de las funciones que se le envian como parametro */
    const [total, users] = await Promise.all([
        // definir el limite y pagina de los registros
        User.countDocuments(filterStatus),
        User.find()
            .skip(parseInt(desde))
            .limit(parseInt(limite))
    ])
    

    res.json({
        total,
        users,
    })
};

const postUsers = async(req, res = response) => {

    const {name, email, password, role} = req.body;
    const user = new User({name, email, password, role});

    // Encriptar contraseña
    const salt = bcryptjs.genSaltSync();
    user.password = bcryptjs.hashSync(password, salt);

    await user.save() // save DB
    data = {
        user,
        nombre: 'post api controller'
    }
    res.json(data)
}

const putUsers = async(req, res = response) => {
    const { id } = req.params
    let { _id, password, google, email, ...others} = req.body

    if (password) {
        // Encriptar contraseña
        const salt = bcryptjs.genSaltSync();
        password = bcryptjs.hashSync(password, salt);

    }
    // findByIdAndUpdate = buscalo por el id y Actualizalo
    const user = await User.findByIdAndUpdate(id,others )
    res.json(user)
}

const patchUsers = (req, res = response) => {
    let data = {
        nombre: 'patch api controller'
    }
    res.json(data)
}
const deleteUsers = async(req, res = response) => {

    const {id} = req.params

    // delete user db
    const user = await User.findByIdAndUpdate(id, {estado:false});
    res.json({user})
    
}



module.exports = {
    getUsers,
    postUsers,
    putUsers,
    patchUsers,
    deleteUsers
}