const { response } = require('express')
const Category = require('../models/category')


// get all categories  || paginate categories
const getCategories = async (req, res = response) => {

    const { limite = 5, desde = 0 } = req.query
    const filterStatus = { status: true }

    // countDocuments=> cuenta la cantidad e categorias activas
    const [total,categories] = await Promise.all([
        Category.countDocuments(filterStatus),
        Category.find(filterStatus)
                .skip(parseInt(desde))
                .limit(parseInt(limite))
                .populate('user','name') // rellena el campo user
        ])

    res.json({
        limite, 
        desde,
        total,
        categories
    })


}

const getByIdCategory = async (req, res = response) => {
    
    const { id } = req.params
    const category = await Category.findById(id).populate('user','name')
    return res.status(200).json({ msg: category })
}

const putCategory = async (req, res = response) => {
    const {id} = req.params
    const name = req.body.name.toUpperCase()
    // {new:true};; visualizar los cambios en la respuesta
    const category = await Category.findByIdAndUpdate(id, { name }, {new:true})

    res.status(200).json({msg: category})

}
const postCategory = async (req, res = response ) => {

    const name = req.body.name.toUpperCase()
    const nameDb = await Category.findOne({name:name})

    if (nameDb){
        return res.status(400).json({
            msg:  `El nombre ${name} ya se encuentra registrado en la base de datos`
        })
     }
     const data = {
         name,
         user: req.user._id
     }
    //  // create categorySchema
     const category = new Category(data)
     // save to db
     category.save()
     res.status(201).json(category)
}

const deleteCategory = async (req, res = response) => {
    const {id} = req.params
    const category = await Category.findByIdAndUpdate(id, {status:false},{new:true})
   res.status(201).json({
       category
    })
}

module.exports = {
    postCategory,
    getCategories,
    getByIdCategory,
    putCategory,
    deleteCategory
}

