const { response } = require("express");
const { User, Category, Product } = require("../models")

const { ObjectId } = require("mongoose").Types

const coleccionesPermitidas = [
    'users',
    'categories',
    'products',
    'roles'
]
const searchUser = async (termino = '', res = response) => {

    const isMongoId = ObjectId.isValid(termino)
    if (isMongoId) {
        const user = await User.findById(termino)
        return res.json({
            results: (user) ? user : []
        })
    }
    // RegExp => expresion regular para buscar todo lo que coincida en mi bd
    const regex = new RegExp(termino,'i')
    const users = await User.find({
        $or: [{name:regex}, {email:regex}],
        $and: [{estado:true}]
    });
    res.json({
        results: users
    })
}
const serchCategories = async (termino, res = response) => {
    const isMongoId = ObjectId.isValid(termino)

    if (isMongoId) {
        const category = await Category.findById(termino)
                                       .populate('user', 'name')
        return res.status(200).json({
            results: (category) ? category : []
        })
    }
    const rgx = new RegExp(termino, 'i')
    const categories = await Category.find({ name:rgx })
                                     .populate('user', 'name')
    res.status(200).json({
        results: (categories) ? categories : []
    })
}
const searchProducts = async (termino, res = response) => {
    const isMongoId = ObjectId.isValid(termino)

    if (isMongoId) {
        const product = await Product.findById(termino)
                                     .populate('user', 'name')
                                     .populate('category','name')
                                     

        return res.status(200).json({
            results: (product) ? product : []
        })
    }
    const regex = new RegExp(termino, 'i')
    const products = await Product.find({name: regex })
                                   .populate('user','name')
                                   .populate('category', 'name')
    res.status(200).json({
        results: (products) ? products : []
    })
}


const search = (req, res = response) => {
    const { coleccion, termino} = req.params;

    if (!coleccionesPermitidas.includes(coleccion)) {
        return res.status(404).json({ 'msg': "not found collection, las colecciones permitidas son " + coleccionesPermitidas })
    }

    switch (coleccion){
        case 'users':
            searchUser(termino, res)
            break;

        case 'categories':
            serchCategories(termino, res)
            break;
        case 'products':
            searchProducts(termino, res)
            break;
        default:
            res.status(404).json({ 'msg':"Se le olvido hacer la busqueda"})
    }
    
}

module.exports = search