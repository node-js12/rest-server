const { response } = require('express')
const Product = require('../models/product')


const getProducts = async (req, res = response) => {

    const { limite = 20, desde = 0 } = req.query

    const query = {status:true}

    const [total, products] = await Promise.all([
        Product.countDocuments(query),
        Product.find(query)
                .populate('user','name')
                .populate('category','name')
                .skip(parseInt(desde))
                .limit(parseInt(limite))
    ])
    res.status(200).json({
        total,
        products
        
    })

}

const getByIdProduct = async (req, res = response) => {
    const { id } = req.params
    const product = await Product.findById(id).populate('user', 'name')
                                              .populate('category','name')
    res.status(200).json(product)
}

const postProduct = async (req, res = response) => {
    const {name,description,price,category} = req.body
    data = {
        name,   
        description,
        price,
        category,
        user: req.user._id
    }
    const product = new Product(data)
    product.save()
    res.status(200).json({ product})

}

const putProduct = async (req, res = response) => {
    const { id } = req.params
    const {user, ...data} = req.body

    const product = await Product.findByIdAndUpdate(id, data, { new: true })
    res.status(201).json({ product })
}

const deleteProduct = async (req, res = response) => {
    const {id} = req.params
    const product = await Product.findByIdAndUpdate(id,{status:false},{new:true})
    res.status(201).json({
        product
    })
}

module.exports = {
    getProducts,
    getByIdProduct,
    postProduct,
    putProduct,
    deleteProduct
}

