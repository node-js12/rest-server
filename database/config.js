const  mongoose = require('mongoose')
require('colors')


const dbConnection = async() => {
    try {
        await mongoose.connect(process.env.CONFIGDB,{
            useNewUrlParser: true, 
            useUnifiedTopology: true,
            useCreateIndex: true,
            useFindAndModify: false
            }) 
        
            console.log('Base de datos corriendo....'+'OK'.blue)
    } 
    catch (e) {
        console.log(e)
        throw new Error('Error a la hora de levantar la BD'.red)
    }
    
}


module.exports = {
    dbConnection
}