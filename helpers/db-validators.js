const Role = require('../models/role')
const User = require('../models/user')
const Category = require('../models/category')
const Product = require('../models/product')


const isValidRol = async(role = '') =>  {

    const roleExist = await Role.findOne( {role} );

    if (!roleExist) {
        throw new Error(`El rol ${role} no está registrado en la BD`)
    }
}

// Verificar si el correo existe
const existEmail = async (email = '') => {
    const email_exist = await User.findOne({email});
    if (email_exist) {
        throw new Error(`El correo ${email} ya se encuentra registrado!`)
    }
} 

const existUserById = async (id = '') => {

    const user_exist = await User.findOne({ '_id':id });

    if (!user_exist) {
        throw new Error(`El id ${id} NO SE encuentra registrado en la BD !`)
    }
} 

const existCategoryById = async (id = '') =>  {
    if (id) {
        const category = await Category.findById(id)
        if (!category) {
            throw new Error(`La categoria ${id} no se encuentra registrado en la BD`)
        }
    }
}

const nameProductExist = async name => {
    const product = await Product.findOne( {name} )
    if (product) {
        throw new Error(`el nombre ${name} ya se encuentra registrado en la BD`)
    }
}

const existProductById = async id => {
    const product = await Product.findById(id)

    if (!product) {
        throw new Error(`El producto ${id} NO se encuentra la BD`)
    }
}

const coleccionesPermitidas = (coleccion,colecciones = []) => {
    const include = colecciones.includes(coleccion);
    
    if (!include) {
        throw new Error(`La coleccion ${coleccion} no es permitida, ${colecciones}`)
    }
    return true
}



module.exports = {
    isValidRol,
    existEmail,
    existUserById,
    existCategoryById,
    nameProductExist,
    existProductById,
    coleccionesPermitidas
}