const path = require('path')
const fs = require('fs')
const { v4: uuidv4 } = require('uuid');
const pathNoImage = path.join(__dirname, "/../assets/notimg.jpg")
const Product = require("../models/product");
const User = require("../models/user");

const subirArchivo = (file, extensionesValidas = ['jpg', 'jpeg', 'png', 'gif'], carpeta = '' ) => {

    return new Promise((resolve, reject) => {

        const { archivo } = file;
        const nameCortado = archivo.name.split('.')
        const extension = nameCortado[nameCortado.length - 1]

        if (!extensionesValidas.includes(extension)) {
            reject(`La extension no es permitida ${extensionesValidas}`)
        }
        // output: idUNico.extension
        const nombreTemp = uuidv4() + '.' + extension
        // unir todo el path queda:  dirActual/uploads/archivo
        const uploadPath = path.join(__dirname + '/../uploads/',carpeta, nombreTemp)

        // Use the mv() method to place the file somewhere on your server
        archivo.mv(uploadPath, (err) => {
        if (err) {
            reject(err)
        }
        resolve(nombreTemp)
      });
   })
}

const notImage = (res) => {
    console.log("entrooooo")
    if (fs.existsSync(pathNoImage)) {
        return res.sendFile(pathNoImage)
    }
   
    return res.json({ "msg": "error del ditectorio", "method": "getImg" })
}

const verificarSiExisteEnBD = (coleccion, id) => {
    return new Promise(async (resolve, reject) => {
        switch (coleccion) {
            case 'users':
                model = await User.findById(id)

                if (!model) {
                    reject({ msg: `No existe un usuario con el id ${id}` })
                }
                break;

            case 'products':
                console.log("product")
                model = await Product.findById(id)
                console.log(model)
                if (!model) {
                    reject({ msg: `No existe un Producto con el id ${id}` })
                }
                break;
        }
        resolve(model)
    })

}

module.exports = {
    subirArchivo,
    notImage,
    verificarSiExisteEnBD

}