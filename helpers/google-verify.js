const {OAuth2Client} = require('google-auth-library');

// get cliente google
const client = new OAuth2Client(process.env.GOOGLE_CLIENT_ID);

// verify valid token cliente
const verifyGoogle = async(idToken = '') => {

  const ticket = await client.verifyIdToken({
      idToken,
      audience: process.env.GOOGLE_CLIENT_ID,  
  });
  const {email, name, picture:img} = ticket.getPayload();

  return {email, name, img}

}


module.exports = {
    verifyGoogle
}