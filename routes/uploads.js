
const { Router } = require('express');
const router = new Router();
const { cargarArchivo, updateImage, getImg, updateImageCloudinary } = require('../controllers/uploads')
const { check } = require('express-validator')
const { validateFields, validateFileUpload } = require('../middleware');
const { coleccionesPermitidas } = require('../helpers/db-validators');

router.get('/:coleccion/:id', [
    check('id', 'No es un id valido de mongo').isMongoId(),
    check('coleccion').custom(c => coleccionesPermitidas(c, ['users', 'products'])),
    validateFields
], getImg)
router.post('/',validateFileUpload,cargarArchivo)
router.put('/:coleccion/:id', [
    validateFileUpload,
    check('id','el id debe ser tipo mongo Id').isMongoId(),
    check('coleccion').custom( c => coleccionesPermitidas(c,['users','products'])),
    validateFields  
], updateImageCloudinary)
// updateImage

module.exports = router;
