const { Router } = require('express');
const { check } = require('express-validator')
const { getCategories, 
        postCategory, 
        getByIdCategory, 
        putCategory, 
        deleteCategory} = require('../controllers/categories');
const { existCategoryById, existNameCategory } = require('../helpers/db-validators');
const { validateFields, validarJWT, isHaveRole } = require('../middleware')
const router = Router()

// get all categories
router.get('/',getCategories)

// get by id category  || public
router.get('/:id', [
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existCategoryById),
    validateFields
], getByIdCategory)

router.post('/', [
    validarJWT,
    check('name','El nombre es obligatorio').not().isEmpty(),
    validateFields

], postCategory)

router.put('/:id', [
    validarJWT,
    check('name', 'el nombre es requerido').not().isEmpty(),
    check('id','No es un id valido').isMongoId(),
    check('id').custom(existCategoryById),
    validateFields
], putCategory)

// superadmin  || change status to false
router.delete('/:id', [
    validarJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existCategoryById),
    isHaveRole('ADMIN_ROLE'),
    validateFields
], deleteCategory)






module.exports = router