const { Router } = require('express');
const { check } = require('express-validator')
const { getProducts, 
        getByIdProduct, 
        postProduct, 
        putProduct,
        deleteProduct } = require('../controllers/products');
const { validateFields, validarJWT, isHaveRole } = require('../middleware')
const { nameProductExist, existCategoryById, existProductById} = require('../helpers/db-validators')
const router = Router()

// get all pruducts
router.get('/', getProducts)

router.get('/:id',[
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existProductById),
    validateFields
], getByIdProduct)

router.post('/',[
    validarJWT,
    check('name', 'El nombre es obligatorio').not().isEmpty(),
    check('name').custom(nameProductExist),
    check('price','El campo precio debe ser numerico').isNumeric(),
    check('description', 'La descripcion es obligatoria').not().isEmpty(),
    check('category', 'La categoria es obligatoria').not().isEmpty(),
    check('category','el id de la categoria es invalido').isMongoId(),
    validateFields

], postProduct)

router.put('/:id',[
    validarJWT,
    check('id', 'id invalido').isMongoId(),
    check('price', 'El campo precio debe ser numerico').isNumeric(),
    check('id').custom(existProductById),   
    check('category').custom(existCategoryById),
    validateFields
], putProduct)

router.delete('/:id', [
    validarJWT,
    check('id','El id es invalido').isMongoId(),
    check('id').custom(existProductById),
    isHaveRole('ADMIN_ROLE'),
    validateFields
], deleteProduct)

module.exports = router