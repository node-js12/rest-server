const { Router } = require('express');
const {getUsers, postUsers, putUsers, patchUsers, deleteUsers} = require('../controllers/users')
const { check } = require('express-validator')
const { isValidRol, existEmail, existUserById }  = require('../helpers/db-validators')
const { validateFields, isHaveRole, validarJWT } = require('../middleware')

const router = new Router();
router.get('/', getUsers )

router.post('/',  
[   
    check('name', 'El nombre es obligatorio').not().isEmpty(),
    check('password', 'El password es obligatorio y debe ser mayor a 7 caracteres').isLength({min:7}),
    check('email','El correo no es válido').isEmail().custom(existEmail),
    check('role').custom(isValidRol),
    validateFields
     
], postUsers);

router.put('/:id', [
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existUserById),
    check('role').custom(isValidRol),
    validateFields
]  ,putUsers);

router.patch('/', patchUsers);

router.delete('/:id', [
    validarJWT,
    // isAdmin
    isHaveRole('ADMIN_ROLE','SALE_ROLE'),
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existUserById),
    validateFields
],  deleteUsers);


module.exports = router;
