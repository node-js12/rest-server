
const { Router } = require('express');
const router = new Router();
const { check } = require('express-validator')
const { login, googleSigin } = require('../controllers/auth')
const { validateFields } = require('../middleware/validar-fileds')

router.post('/login', [
    check('email', 'El correo es obligatorio').not().isEmpty(),
    check('email', 'Ingresa un email válido').isEmail(),
    check('password', 'La contraseña es obligatoria').not().isEmpty(),
    validateFields
], login);

router.post('/google', [
    check('id_token', 'El id_token es obligatorio').not().isEmpty(),
    validateFields
], googleSigin);




module.exports = router;
