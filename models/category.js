const { Schema, model } = require('mongoose')

const CategorySchema = Schema({
    name: {
        type: String,
        required: [true, 'El nombre es requerido'],
        unique: true
    },
    status: {
        type: Boolean,
        default: true
    },
    // hacemos referencia a quien creó la categoria
    user:{
        // tipo de dato id de mongo
        type: Schema.Types.ObjectId,
        // referencia a la entidad mongo
        ref: 'User' ,
        required: true
    }
})
CategorySchema.methods.toJSON = function () {

    // ocultar las contraseña en el api rest
    const { __v, status, ...categories } = this.toObject();
    return categories
}


module.exports = model('Category', CategorySchema)