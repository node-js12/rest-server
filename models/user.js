const { Schema, model } = require('mongoose')


const UserSchema = Schema({ 
    name: { 
        type: 'string',
        required: [true, 'El nombre es obligatorio'],
    },
    email: {
        type: 'string',
        required: [true, 'El correo es obligatorio'],
        unique: true
    },
    password: {
        type: 'string',
        required: [true, 'La contraseña es obligatorio']
    },
    img: {
        type: 'string',
    },
    role: {
        type: 'string',
        default: 'USER_ROLE',
        enum: ['ADMIN_ROLE', 'USER_ROLE','SALE_ROLE']
    },
    estado: {
        type: Boolean,
        default: true
    },
    google: {
        type: Boolean,
        default: false
    }
})

UserSchema.methods.toJSON = function() {
    // ocultar las contraseña en el api rest
    const { __v, password, _id,...users } = this.toObject();
    users["uid"] = _id
    return users
}

module.exports = model('User', UserSchema)