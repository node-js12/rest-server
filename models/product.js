const { Schema, model } = require('mongoose')

const ProductSchema = Schema({
    name: {
        type: String,
        required: [true, 'El nombre es requerido']
    },
    status: {
        type: Boolean,
        default: true
    },
    // hacemos referencia a quien creó la categoria
    user: {
        // tipo de dato id de mongo
        type: Schema.Types.ObjectId,
        // referencia a la entidad mongo
        ref: 'User',
        required: true
    },
    price:{type: Number,default:0},
    description:{type:String, required:true},
    category:{
        type: Schema.Types.ObjectId,
        ref: 'Category',
        required: true
    },
    available: {type:Boolean,default:true},
    img: {type:String, required:false}
})
ProductSchema.methods.toJSON = function () {

    // ocultar las contraseña en el api rest
    const { __v, ...data } = this.toObject();
    return data
}


module.exports = model('Product', ProductSchema)