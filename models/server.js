const express = require('express')
require('colors')
const cors = require('cors')
const {dbConnection} = require('../database/config')
const fileUpload = require('express-fileupload')


class Server {
    
    constructor() {
        this.port = process.env.PORT
        this.app = express()
        this.paths = {
            auth: '/api/auth',
            search: '/api/search',
            user: '/api/users',
            category: '/api/categories',
            product: '/api/products',
            uploads: '/api/uploads'
        }
 
        // connect to db
        this.connectDB()
        // middlewares => añaden funcionalidad a mi app
        this.middlewares()
        // routes app.
        this.routes()
    }

    async connectDB() {
        await dbConnection()
    }

    middlewares() {

        // cors
        this.app.use(cors())

        // parseo y lectura del body
        this.app.use(express.json())

        // use => inicialize middleware (/)
        this.app.use(express.static('public'))

        // manejar la carga de archivos
        this.app.use(fileUpload({
            useTempFiles: true,
            tempFileDir: '/tmp/',
            // crea automaticamente el dir
            createParentPath: true
        }));
    }

    routes() {
        this.app.use(this.paths.auth, require('../routes/auth'))  
        this.app.use(this.paths.user, require('../routes/users'))  
        this.app.use(this.paths.product, require('../routes/products'))
        this.app.use(this.paths.category, require('../routes/categories'))
        this.app.use(this.paths.search, require('../routes/search'))
        this.app.use(this.paths.uploads, require('../routes/uploads'))
    }

    listen() {
        this.app.listen(this.port, () => {
            console.log('Corriendo en el puerto.....', this.port.brightBlue)
        })
    }
}

module.exports = Server;